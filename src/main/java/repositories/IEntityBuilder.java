package repositories;

import java.sql.ResultSet;
import java.sql.SQLException;

import mysql.Entity;

public interface IEntityBuilder<TEntity extends Entity> {

	public TEntity build(ResultSet rs) throws SQLException;
	
}