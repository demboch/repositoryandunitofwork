package repositories.repositoriesImp;

import java.util.List;
import unitofwork.*;
import mysql.Entity;
import mysql.EnumerationValue;

public class MySqlEnumerationValuesRepository implements EnumerationValueRepository, IUnitOfWorkRepository {

	public EnumerationValue withId(int id) {
		return null;
	}

	public void allOnPage(PagingInfo page) {}

	public void save(EnumerationValue entity) {}

	public void delete(EnumerationValue entity) {}

	public void update(EnumerationValue entity) {}

	public int count() {
		return 0;
	}

	public List<EnumerationValue> withName(String name) {
		return null;
	}

	public EnumerationValue withIntKey(int key, String name) {
		return null;
	}

	public EnumerationValue withStringKey(String key, String name) {
		return null;
	}

	public void persistAdd(Entity entity) {}

	public void persistUpdate(Entity entity) {}

	public void persistDelete(Entity entity) {}

	public EnumerationValue get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<EnumerationValue> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
