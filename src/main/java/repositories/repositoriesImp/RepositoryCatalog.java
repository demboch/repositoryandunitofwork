package repositories.repositoriesImp;

import java.sql.Connection;

import check.RuleChecker;
import mysql.*;
import repositories.*;
import unitofwork.IUnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog {

	private Connection connection;
	private IUnitOfWork uow;
	private RuleChecker<User> userChecker = new RuleChecker<User>();
	private RuleChecker<Person> personChecker = new RuleChecker<Person>();
	
	public RuleChecker<User> getUserChecker() {
		return userChecker;
	}

	public RuleChecker<Person> getPersonChecker() {
		return personChecker;
	}

	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}

	public IUserRepository getUsers() {
		return new UserRepository(connection, new UserBuilder(), uow);
	}

	public IRepository<UserRoles> getRoles() {
		return null;
	}

	public void commit() {
		uow.saveChanges();
	}

	public IRepository<Person> getPersons() {
		return new PersonRepository(connection, new PersonBuilder(), uow);
	}

}