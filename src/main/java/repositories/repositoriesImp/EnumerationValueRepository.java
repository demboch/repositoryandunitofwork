package repositories.repositoriesImp;

import java.util.List;

import mysql.EnumerationValue;
import repositories.IRepository;

public interface EnumerationValueRepository extends IRepository<EnumerationValue>{

	public List<EnumerationValue> withName(String name);
	public EnumerationValue withIntKey(int key, String name);
	public EnumerationValue withStringKey(String key, String name);
}
