package repositories;

import java.util.List;

import repositories.repositoriesImp.PagingInfo;

public interface IRepository<TEntity> {

	public TEntity get(int id);
	public List<TEntity> getAll();
	public void save(TEntity entity);
	public void delete(TEntity entity);
	public void update(TEntity entity);
	
//	public void allOnPage(PagingInfo page);
//	public int count();	
}