package repositories;

import mysql.Person;
import mysql.UserRoles;
//import repositories.repositoriesImp.EnumerationValueRepository;

public interface IRepositoryCatalog {

	public IUserRepository getUsers();
	public IRepository<Person> getPersons();
	public IRepository<UserRoles> getRoles();
	public void commit();
	
}

//public EnumerationValueRepository enumerations();
//public IUserRepository users();
