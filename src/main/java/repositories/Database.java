package repositories;

import mysql.EnumerationValue;
import mysql.RolesPermissions;
import mysql.UserRoles;
import mysql.User;

import java.util.HashSet;
import java.util.Set;

public class Database {

    public Set<User> Users = new HashSet<User>();
    public Set<EnumerationValue> EnumerationValues = new HashSet<EnumerationValue>();
    public Set<UserRoles> Roles = new HashSet<UserRoles>();
    public Set<RolesPermissions> RolePermissions = new HashSet<RolesPermissions>();
    public void saveChanges() {  }
    public void undo() {  }
}