package repositories;

import java.util.List;

import mysql.*;

public interface IUserRepository extends IRepository<User>{

	public List<User> withRole(UserRoles role);
	public List<User> withRole(String roleName);
	public List<User> withRole(int roleId);
	
//	public User withLogin(String login);
//	public User withLoginAndPassword(String login, String password);
//	public void setupPermission(User user);
		
}


