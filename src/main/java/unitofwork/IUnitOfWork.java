package unitofwork;

import mysql.Entity;

public interface IUnitOfWork {

    void saveChanges(); // commit
    void undo(); // rollback

    void markAsNew(Entity entity, IUnitOfWorkRepository repository);
    void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);
    void markAsChanged(Entity entity, IUnitOfWorkRepository repository);
}