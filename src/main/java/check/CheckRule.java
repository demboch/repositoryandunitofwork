package check;

import mysql.Entity;

public interface CheckRule<TEntity extends Entity> {

	public CheckResult checkRule(TEntity entity);
}
