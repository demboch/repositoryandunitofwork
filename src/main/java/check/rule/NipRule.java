package check.rule;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import mysql.Person;

public class NipRule implements CheckRule<Person> {

	CheckResult cr = new CheckResult();

	public CheckResult checkRule(Person entity) {
		if (entity.getNip() == null) {
			cr.setMessage("NIP nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getNip().length() != 10) {
			cr.setMessage("NIP za dlugi, lub za krotki");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		char[] nipArray = entity.getNip().toCharArray();
		for (int i = 0; i < nipArray.length; i++) {
			if (!(nipArray[i] >= 48 && nipArray[i] <= 57)) {
				cr.setMessage("NIP nie moze zawierac liter");
				cr.setResult(RuleResult.Error);
				return cr;
			}
		}

		int[] weights = { 6, 5, 7, 2, 3, 4, 5, 6, 7 };
		int sum = 0;
		for (int i = 0; i < 9; i++) {
			sum += Character.getNumericValue(nipArray[i]) * weights[i];
		}
		sum %= 11;

		if (sum != Character.getNumericValue(nipArray[9])) {
			cr.setMessage("Nie prawidlowa suma kontrolna");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		cr.setMessage("NIP jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}
}
