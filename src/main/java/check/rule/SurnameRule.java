package check.rule;

import check.CheckResult;
import check.CheckRule;
import check.RuleResult;
import mysql.Person;

public class SurnameRule implements CheckRule<Person> {

	CheckResult cr = new CheckResult();
	
	public CheckResult checkRule(Person entity) {
		if (entity.getSurname() == null) {
			cr.setMessage("Surname nie może być  nullem");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		if (entity.getSurname().equals("abcdef")) {
			cr.setMessage("Surname nieprawidlowy");
			cr.setResult(RuleResult.Error);
			return cr;
		}

		cr.setMessage("Surname jest OK");
		cr.setResult(RuleResult.Ok);

		return cr;
	}
}

