package check;

import java.util.ArrayList;
import java.util.List;

import mysql.Entity;

public class RuleChecker<TEntity extends Entity> {

	private List<CheckRule<TEntity>> rules = new ArrayList<CheckRule<TEntity>>();
	
	public void addRule(CheckRule<TEntity> rule){
		rules.add(rule);	
	}
	
	public List<CheckResult> check(TEntity entity)
	{
		List<CheckResult> listResults = new ArrayList<CheckResult>();		
		
		for (CheckRule<TEntity> rule : rules)
		{
			CheckResult result = rule.checkRule(entity);
			listResults.add(result);
		}
		
		return listResults;
	}

}
