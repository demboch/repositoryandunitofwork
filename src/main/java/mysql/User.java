package mysql;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity {

	private String login;
	private String password;

	private List<UserRoles> roles;
	private List<RolesPermissions> permissions;
	
	private Person person;

	public User() {
		roles = new ArrayList<UserRoles>();
		permissions = new ArrayList<RolesPermissions>();
	}

	public List<RolesPermissions> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<RolesPermissions> permissions) {
		this.permissions = permissions;
	}

	public List<UserRoles> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRoles> roles) {
		this.roles = roles;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
		if (!this.equals(person.getUser()))
			person.setUser(this);
	}
}