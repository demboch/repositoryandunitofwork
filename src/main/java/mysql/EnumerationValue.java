package mysql;

public class EnumerationValue extends Entity {

	private int intKey;
	private String stringkey;
	private int value;
	private int enumerationName;

	public int getIntKey() {
		return intKey;
	}

	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}

	public String getStringkey() {
		return stringkey;
	}

	public void setStringkey(String stringkey) {
		this.stringkey = stringkey;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getEnumerationName() {
		return enumerationName;
	}

	public void setEnumerationName(int enumerationName) {
		this.enumerationName = enumerationName;
	}
}
