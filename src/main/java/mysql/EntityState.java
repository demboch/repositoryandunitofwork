package mysql;

public enum EntityState {

	New, Modified, UnChanged, Deleted, Unknown;
}
