package mysql;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person extends Entity {

	private String firstName;
	private String surname;
	private String pesel;
	private String nip;
	private String email;
	private Date dateOfBirth;

	private User user;

	private List<PhoneNumber> phoneNumber;
	private List<Address> adsress;

	public Person() {
		phoneNumber = new ArrayList<PhoneNumber>();
		adsress = new ArrayList<Address>();
	}

	public List<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(List<PhoneNumber> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Address> getAdsress() {
		return adsress;
	}

	public void setAdsress(List<Address> adsress) {
		this.adsress = adsress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		if (!this.equals(user.getPerson()))
			user.setPerson(this);
		this.user = user;
	}
}