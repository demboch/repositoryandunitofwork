package tests;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.DateOfBirthRule;
import mysql.Person;

public class DateOfBirthRuleTest {

	DateOfBirthRule rule= new DateOfBirthRule();
	Date date = new Date();
	Person person = new Person();

	@Test
	public void should_return_error_for_pesel_equal_to_null() {
		person.setPesel(null);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void checker_will_return_ok_if_date_is_not_after_now(){
		person.setDateOfBirth(date);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
