package tests;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.SurnameRule;
import mysql.Person;

public class SurnameRuleTest {

	SurnameRule rule = new SurnameRule();
	Person person = new Person();

	@Test
	public void should_return_error_for_surname_equal_to_null() {
		person.setSurname(null);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_incorrect_surname() {
		person.setFirstName("abcdef");
		CheckResult actual = rule.checkRule(person);
		
		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void should_return_ok_for_correct_surname() {
		person.setSurname("Kowalski");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
