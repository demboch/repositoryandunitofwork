package tests;

import org.junit.Assert;
import org.junit.Test;

import check.CheckResult;
import check.RuleResult;
import check.rule.FirstNameRule;
import mysql.Person;

public class FirstNameRuleTest {

	FirstNameRule rule = new FirstNameRule();
	Person person = new Person();
	
	@Test
	public void should_return_error_for_first_name_equal_to_null() {
		person.setFirstName(null);
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}

	@Test
	public void should_return_error_for_incorrect_first_name() {
		person.setFirstName("abcdef");
		CheckResult actual = rule.checkRule(person);
		
		Assert.assertEquals(RuleResult.Error, actual.getResult());
	}
	
	@Test
	public void should_return_ok_for_correct_first_name() {
		person.setFirstName("Maciej");
		CheckResult actual = rule.checkRule(person);

		Assert.assertEquals(RuleResult.Ok, actual.getResult());
	}
}
